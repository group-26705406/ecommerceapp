from django.urls import path, include

from . import views

app_name="core"

urlpatterns = [
    path("",views.index, name='index'),
    #empty string implies no url pattern thus first page
    path("about/",views.about, name='about'),
    path("base/",views.base),
    path("search/",views.search, name="search"),
    path("login/",views.login_user, name="login"),
    path("logout/",views.logout_user, name="logout"),
    path("register/",views.register_user, name="register"),
    path("product/<int:pk>",views.product,name="product"),
    path("category/<str:foo>",views.category, name="category"),
    path('vendors/<int:pk>', views.vendor_detail, name='vendor_detail'),
    path('business_register/',views.business_register, name='business_register'),
    #path('myaccount/',views.myaccount, name='myaccount'),
    path('mystore/',views.mystore, name='mystore'),
    path('mystore/addproduct',views.addproduct, name='addproduct'),
    path("category_summary/",views.category_summary, name="category_summary"),
    path("myproduct/",views.myproduct,name="myproduct"),
    path("myproduct/editproduct/deleteProduct/<int:pk>",views.deleteProduct,name="deleteProduct"),
    path('myproduct/editproduct/<int:pk>/', views.editproduct, name='editproduct'),
    path("allproduct/",views.allproduct,name="allproduct"),
    path("update_user/",views.update_user, name="update_user"),
    path("update_password/",views.update_password, name="update_password"),
   
]
  