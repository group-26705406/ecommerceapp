import logging
from django.shortcuts import render, redirect
from product.models import Product, Category
from django.http import HttpResponse
from userprofile.models import Userprofile
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django import forms
from .forms import SignUpForm, SignUpForm2, UpdateUserForm, ChangePasswordForm
from django.db.models import Q
from product.forms import ProductForm
from django.utils.text import slugify


def index(request):
    products= Product.objects.all()
    categories=Category.objects.all()
    return render(request, 'core/index.html',{'products':products, 'categories':categories})

def update_password(request):
    if request.user.is_authenticated:
        current_user = request.user
        #did user fill in the form
        if request.method == 'POST':
            form = ChangePasswordForm(current_user, request.POST)
            #is form valid
            if form.is_valid():
                form.save()
                messages.success(request, "Password has been updated...")
                login(request, current_user)
                return redirect('core:update_password')
            
            else:
                for error in list(form.errors.values()):
                    messages.error(request, error)
                    return redirect('core:update_password')
                
        else:
            form = ChangePasswordForm(current_user)
            return render(request, 'core/update_password.html', {'form':form})

    else:
       messages.success(request, "Login required to Update Password...")
       return redirect('core:index')  
       

def update_user(request):
    if request.user.is_authenticated:
        current_user = User.objects.get(id=request.user.id)
        user_form = UpdateUserForm(request.POST or None, instance=current_user)
        
        if user_form.is_valid():
            user_form.save()
            
            login(request, current_user)
            messages.success(request, "User has been Updated...")
            return redirect('core:index')
        return  render(request,'core/update_user.html',{'user_form':user_form})
    else:
        messages.success(request, "Login required to Update Profile...")
        return redirect('core:index')   

def category_summary(request):
    categories=Category.objects.all()
    return render(request,'core/category_summary.html',{'categories':categories})

def allproduct(request):
    products= Product.objects.all()
    return render(request, 'core/allproduct.html',{'products':products})

 
def about(request):
    return render(request, 'core/about.html', {})

def addproduct(request):
    return render(request, 'core/addproduct.html', {})



def myproduct(request):
    buss_id=request.user.id
    user = User.objects.get(pk=buss_id)
    return render(request, 'core/myproduct.html', {'user':user})
  

def product(request,pk):
    product= Product.objects.get(id=pk)
    return render(request, 'core/product.html',{'product':product})
# view for the homepage

def vendor_detail(request, pk):
    user = User.objects.get(pk=pk)
    return render(request, 'core/vendor_detail.html', {'user':user})


def product_detail(request, slug):
    product = Product.objects.get(slug=slug)
    return render(request, 'core/product_details.html',{'product':product})
# view for the product detail page


def base(request):
    return render(request, 'core/base.html') 

@login_required
def mystore(request):
    return render(request, 'core/mystore.html') 


def editproduct(request,pk):
    # Getting the product 
    product = Product.objects.get(id=pk)
    
    if request.method == 'POST':
        form = ProductForm(request.POST,request.FILES, instance=product)
        product.delete()
        if form.is_valid():
            product= form.save(commit=False)
            product.save()
            messages.success(request, "Product added successfully!")
            return redirect('core:myproduct')
    else:
        # If the request is GET, render the form pre-filled with product details
        form = ProductForm(instance=product)
    categories = Category.objects.all() 
    # Render the template with the form
    return render(request, 'core/editproduct.html', {'form': form, 'product': product, 'categories': categories})

def deleteProduct(request,pk):
    product = Product.objects.get(id=pk)
    product.delete()
    return render(request,'core/myproduct.html')

def addproduct(request):
    if request.method == "POST":
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            product = form.save(commit=False)
            product.user = request.user
            product.save()
            messages.success(request, "Product added successfully!")
            return redirect('core:myproduct')
        else:
            messages.error(request, "Form validation failed. Please correct the errors.")
    else:
        form = ProductForm() 
    categories = Category.objects.all()  
    return render(request, 'core/addproduct.html', {'form': form, 'categories': categories})


@login_required
#def myaccount(request):
    #return render(request, 'core/myaccount.html')

def search(request):
    query = request.GET.get('query','')
    products= Product.objects.filter(title__icontains=query)
    return render(request,'core/search.html',{'query':query,'product':products})

def register_user(request):
    form = SignUpForm()
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()  # Save the form data to create a new user object
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, "You have been registered!")
            return redirect('core:index')
        else:
            messages.error(request, "There was an error! Please try again.")
    return render(request, 'core/register.html', {'form': form})

def business_register(request):
    form=SignUpForm2()
    if request.method== "POST":
        form=SignUpForm2(request.POST)
        #if form has been filled
        if form.is_valid():
            user = form.save()  # Save the form data to create a new user object
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, "You have been registered!")
            return redirect('core:index')
        else:
            messages.error(request, "There was an error! Please try again.")
    return render(request, 'core/business_register.html',{'form':form}) 

#view for login_user
def login_user(request):
    if request.method=="POST":
        # filing the form is "post", so if form is filled
        username=request.POST['username']
        password=request.POST['password']
        user    =authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request,("You have been logged in!"))
            return redirect('core:index')
        else:
            messages.success(request,("There was an error!"))
        return render(request, 'core/login.html',{}) 
    else:
        return render(request, 'core/login.html',{}) 


def logout_user(request):
    logout(request)
    messages.success(request,("You have been logged out!"))
    return redirect('core:index') 
    

def category(request, foo):
    #replace hyphens
    foo= foo.replace('-', ' ')
    #grab category from url
    try:
        #look up category
        category = Category.objects.get(name=foo) 
        products = Product.objects.filter(category=category)  
        return render(request, 'core/category.html',{'products':products, 'category':category})
    except:
      messages.success(request,("That category doesn't exist...."))
      return redirect('core:index') 
      