from django import forms

from .models import Product
#from .models import ProductReview

class ProductForm(forms.ModelForm):
    class Meta:
        model=Product
        fields = {'category','title', 'description','image','price',}