from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .models import Product, Review
from .forms import ReviewForm

# Create your views here.
def product_details(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    reviews = Review.objects.filter(product=product)
    form = ReviewForm()
    
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.product = product
            review.save()
            return HttpResponseRedirect(request.path)

    return render(request, 'product_details.html', {'product': product, 'reviews': reviews, 'form': form})
# Create your views here.

