from django.db import models
from django.db import models
from django.contrib.auth.models import User

import datetime

class Category(models.Model):
    name=models.CharField(max_length=50)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural="Categories"
    
class Customer(models.Model):
    first_name=models.CharField(max_length=50)
    last_name=models.CharField(max_length=50)
    phone=models.CharField(max_length=50)
    email=models.EmailField(max_length=100)
    password=models.CharField(max_length=50)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class Product(models.Model):
    user    = models.ForeignKey(User, related_name='products',on_delete=models.CASCADE)
    category= models.ForeignKey(Category, related_name='products',on_delete=models.CASCADE)
    title      = models.CharField(max_length=20)
    description= models.TextField(blank=True, null= True)
    price      = models.IntegerField(blank=True)
    image      =models.ImageField(upload_to='upload/product/')
    created_at =models.DateTimeField(auto_now_add=True)
    updated_at =models.DateTimeField(auto_now=True)

    class Meta:
        ordering=('-created_at',)

    def __str__(self):
        return self.title

class Order(models.Model):
    product  =models.ForeignKey(Product, on_delete=models.CASCADE)
    customer =models.ForeignKey(Customer, on_delete=models.CASCADE)
    quantity =models.IntegerField(default=1)
    address  =models.CharField(max_length=100,default='', blank=True)
    phone    =models.CharField(max_length=100,default='', blank=False)
    date     =models.DateField(default=datetime.datetime.today)
    status   =models.BooleanField(default= False)

    def __str__(self):
        return self.product
# Create your models here.
