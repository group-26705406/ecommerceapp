from django.shortcuts import render
from product.models import Product
from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
import uuid
from django.urls import reverse
from cart.cart import Cart
from product.models import Product

def CheckOut(request):#, product_id):
    product=Product(request)
#product = Product.objects.get(id = product_id)
#Get the cart
    cart = Cart(request)
    cart_products = cart.get_products
    quantities = cart.get_quantities
    totals = cart.cart_total()
    
    host = request.get_host()
    
    paypal_checkout = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': product.price,
        'item_name': product.title,
        'invoice': uuid.uuid4(),
        'currency_code': 'UGX',
        'notify_url': f"https://{host}{reverse('paypal-ipn')}",
        #'return_url' : f "http://{host}{reverse('PaymentApp:payment-success')}", #, kwargs = {'product_id' : product.id }) } ",
        #'cancel_url' : f "http://{host}{reverse('PaymentApp:payment-failed')}", #, kwargs = {'product_id' : product.id }) } ",                        
    }
    
    paypal_payment = PayPalPaymentsForm(initial=paypal_checkout)
    
    #context ={
        #'product' : product,
        #'paypal': paypal_payment
    #}
    return render(request, 'core/checkout.html', {"cart_products":cart_products, "quantities":quantities, "totals":totals, 'paypal':paypal_payment})
#return render(request, 'core/checkout.html', context)

def PaymentSuccessful(request, cart_products, quantities, totals):#, product_id):
    #product = Product.objects.get(id=product_id)
    return render(request, 'core/payment-success.html', {"cart_products":cart_products, "quantities":quantities, "totals":totals})

def paymentFailed(request, cart_products, quantities, totals):#, product_id):
    #product = Product.objects.get(id=product_id)
    return render(request, 'core/payment-failed.html', {"cart_products":cart_products, "quantities":quantities, "totals":totals})