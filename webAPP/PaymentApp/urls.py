from django.urls import path
from . import views

app_name="PaymentApp"

urlpatterns = [
    path('checkout/', views.CheckOut, name='checkout'),
    # path('payment-success/', views.PaymentSuccessful, name='payment-success'),
    # path('payment-failed/', views.PaymentSuccessful, name='payment-failed'), 
    ]