from django.urls import path, include
from . import views

app_name="cart"

urlpatterns = [
    path("", views.cart_summary, name="cart_summary"), #main url pattern for the cart
    path('add/', views.cart_add, name="cart_add"), #url pointing to add page
    path('delete/', views.cart_delete, name="cart_delete"), #url pointing to delete page
    path('update/', views.cart_update, name="cart_update"), #url pointing to update page
]
  