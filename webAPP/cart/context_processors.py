from . cart import Cart

#context processor such that the cart works on all site pages
def cart(request):
    return {'cart': Cart(request)}  #return default data from the cart
