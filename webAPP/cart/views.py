from django.shortcuts import render, get_object_or_404
from .cart import Cart
from product.models import Product
from django.http import JsonResponse
from django.contrib import messages


def cart_summary(request):
    #Get the cart
    cart = Cart(request)
    cart_products = cart.get_products
    quantities = cart.get_quantities
    totals = cart.cart_total()
    return render(request, 'core/cart_summary.html', {"cart_products":cart_products, "quantities":quantities, "totals":totals}) #returns the cart_summary.html template
    #return render(request, 'core/cart_summary.html', {})


def cart_add(request):
    #Get the cart
    cart = Cart(request)
    #test for post
    if request.POST.get('action') == 'post':
        #Get stuff
        product_id = int(request.POST.get('product_id'))
        product_qty = str(request.POST.get('product_qty'))
        #lookup product in the database
        product = get_object_or_404(Product, id=product_id)
        
        #save to session
        cart.add(product=product, quantity=product_qty)
        
        #Get cart quantity
        cart_quantity = cart.__len__()
        
        #return response
        #response=JsonResponse({'Product title: ': product.title})
        response=JsonResponse({'qty: ': cart_quantity})
        messages.success(request,("Product added to Cart..."))
        return response


def cart_delete(request):
    cart = Cart(request)
    if request.POST.get('action') == 'post':
        #Get stuff
        product_id = int(request.POST.get('product_id'))
        # Call the delete function in Cart
        cart.delete(product=product_id)
        
        response = JsonResponse({'product': product_id})
        #return redirect('cart_summary')
        messages.success(request,("Product deleted from Cart..."))
        return response

def cart_update(request):
    cart = Cart(request)
    if request.POST.get('action') == 'post':
        #Get stuff
        product_id = str(request.POST.get('product_id'))
        product_qty = str(request.POST.get('product_qty'))
        
        cart.update(product=product_id, quantity=product_qty)
        
        response = JsonResponse({'qty': product_qty})
        #return redirect('cart_summary')
        messages.success(request,("Cart updated..."))
        return response
