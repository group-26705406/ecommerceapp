from product.models import Product


class Cart():
    def __init__(self, request): #request to view the cart page and create a sesssion for a user
        self.session = request.session
        
        #get the current session key if it exists
        cart = self.session.get('session_key')
        
        #for a new user, create a new session key instead
        if 'session_key' not in request.session:
            cart = self.session['session_key'] =  {}
            
        self.cart = cart #avails the cart on all pages of site    
        
    
    def add(self, product, quantity):
        product_id = str(product.id)
        product_quantity = str(quantity)
        if product_id in self.cart:
            pass
        else:
            #self.cart[product_id] = {'price': str(product.price) }  
            self.cart[product_id] = str(product_quantity)  
            
        self.session.modified = True
        
    def cart_total(self):
        # GEt product IDS
        product_ids = self.cart.keys()
        # lookup the keys in our products database models
        products = Product.objects.filter(id__in=product_ids)
        
        #get quantities
        quantities = self.cart
    
        if None in quantities.keys():
            del quantities["None"]
        
        print(quantities)
        # Start counting at 0
        total = 0
        for key, value in quantities.items():
            key = int(key)
            for product in products:
                if product.id == key:
                    # if product.is_sale:
                    #     total = total + (product.sale_price * value)
                    # else:
                        total = total + (product.price * int(value))
                        
        return total
        
        
        
    def __len__(self):
        return len(self.cart)
    
    def get_products(self):
        # Get ids from cart
        product_ids = self.cart.keys()
        # Use ids to lookup products in database model
        products = Product.objects.filter(id__in=product_ids)
        # Return the looked up products
        return products
    
    def get_quantities(self):
        quantities = self.cart
        return quantities
    
    def update(self, product, quantity):
        product_id = str(product)
        product_quantity = str(quantity)
                
        #Get Cart
        ourcart = self.cart
        #Update cart
        ourcart[product_id] = product_quantity
        
        self.session.modified = True
        
        thing = self.cart
        return thing
    
    def delete(self, product):
        product_id = str(product)
        # Delete from cart
        if product_id in self.cart:
            del self.cart[product_id]
            
        self.session.modified = True   

        