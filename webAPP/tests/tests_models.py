from django.test import TestCase
from product.models import *
from django.contrib.auth.models import User
from datetime import date
from PaymentApp.models import *
import pytest

pytestmark = pytest.mark.django_db
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'webpp.settings'

class CategoryTest(TestCase):

    def test_category_creation(self):
        category = Category.objects.create(name="Test Category")
        self.assertEqual(category.name, "Test Category")

    def test_category_string_representation(self):
        category = Category.objects.create(name="Test Category")
        self.assertEqual(str(category), "Test Category")

class CustomerTest(TestCase):

    def test_customer_creation(self):
        customer = Customer.objects.create(
            first_name="Seremba",
            last_name="Moses",
            phone="0789625348",
            email="serembamoses@example.com",
            password="moses1234",
        )

        self.assertEqual(customer.first_name, "Seremba")
        self.assertEqual(customer.last_name, "Moses")
        self.assertEqual(customer.phone, "0789625348")
        self.assertEqual(customer.email, "serembamoses@example.com")

    def test_customer_string_representation(self):
        customer = Customer.objects.create(
            first_name="Seremba",
            last_name="Moses",
            phone="0789625348",
            email="serembamoses@example.com",
            password="testpassword",
        )

        self.assertEqual(str(customer), "Seremba Moses")


class ProductTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username="test_user", password="test123")
        self.category = Category.objects.create(name="Test Category")

    def test_product_creation(self):
        product = Product.objects.create(
            user=self.user,
            category=self.category,
            title="Test Product",
            price=167000,
        )

        self.assertEqual(product.user, self.user)
        self.assertEqual(product.category, self.category)
        self.assertEqual(product.title, "Test Product")
        self.assertEqual(product.price, 167000)

    def test_product_string_representation(self):
        product = Product.objects.create(
            user=self.user,
            category=self.category,
            title="Test Product",
            price=167000,
        )

        self.assertEqual(str(product), "Test Product")


class OrderTest(TestCase):

    def set_up(self):
        self.customer = Customer.objects.create(
            first_name="Seremba",
            last_name="Moses",
            phone="0789625348",
            email="nancyrose@example.com",
            password="rose123456",
        )
        self.category = Category.objects.create(name="Test Category")
        self.product = Product.objects.create(
        user=User.objects.create_user(username="nancyrose", password="rose123456"),
        category=self.category,
        title="Test Product",
        price=167000,
        )

    def test_order_creation(self):
        order = Order.objects.create(
            product=self.product,
            customer=self.customer,
            quantity=2,
            address="Makerere Kikoni",
            phone="0789667348",
            date=date.today(),
            status=True,
        )

        self.assertEqual(order.product, self.product)
        self.assertEqual(order.customer, self.customer)  # Corrected line
        self.assertEqual(order.quantity, 2)
        self.assertEqual(order.address, "Makerere Kikoni")
        self.assertEqual(order.phone, "0789667348")
        self.assertEqual(order.date, date.today())
        self.assertEqual(order.status, True)
        
class OrderCreationTest(TestCase):
    def set_up(self):
        self.user = User.objects.create_user(username="rodrigonzalez", password="gonzalez1234")
        self.product = Product.objects.create(
            title="Test Product",
            price=167000,
        )

    def test_order_creation(self):
        order = Order.objects.create(
            user=self.user,
            full_name="serembamoses",
            email="serembamoses@example.com",
            shipping_address="Makerere Kikoni",
            amount_paid=167000,
            date_ordered=datetime.now(),
        )

        self.assertEqual(order.user, self.user)
        self.assertEqual(order.full_name, "Seremba moses")
        self.assertEqual(order.email, "serembamoses@example.com")
        self.assertEqual(order.shipping_address, "Makerere Kikoni")
        self.assertEqual(order.amount_paid, 167000)

    def test_order_string_representation(self):
        order = Order.objects.create(
            user=self.user,
            full_name="Seremba Moses",
            email="serembamoses@example.com",
            shipping_address="Makerere Kikoni",
            amount_paid=167000,
            date_ordered=datetime.now(),
        )

        self.assertEqual(str(order), f"Order - {str(order.id)}")
    

class Order_Item_Test(TestCase):
    def set_up(self):
        self.user = User.objects.create_user(username="test_user", password="test123")
        self.product = Product.objects.create(
            title="Test Product",
            price=167000,
        )
        self.order = Order.objects.create(user=self.user)

    def test_order_item_creation(self):
        order_item = OrderItem.objects.create(
            order=self.order,
            product=self.product,
            user=self.user,
            quantity=2,
            price=self.product.price,
        )

        self.assertEqual(order_item.order, self.order)
        self.assertEqual(order_item.product, self.product)
        self.assertEqual(order_item.user, self.user)
        self.assertEqual(order_item.quantity, 2)
        self.assertEqual(order_item.price, self.product.price)

    def test_order_item_string_representation(self):
        order_item = OrderItem.objects.create(
            order=self.order,
            product=self.product,
            user=self.user,
            quantity=2,
            price=self.product.price,
        )

        self.assertEqual(str(order_item), f"Order Item - {str(order_item.id)}")