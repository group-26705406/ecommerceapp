from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed
from django.test import TestCase, Client
from product.models import Product, Category, User  
from core.views import *
from django.contrib.messages import get_messages
from django.test import RequestFactory
import pytest

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'webpp.settings'
pytestmark = pytest.mark.django_db

class CoreViewTest(TestCase):
    
    def set_up(self):
        self.client = Client()
        # Create some sample data for testing (modify as needed)
        self.category = Category.objects.create(name="Test Category")
        self.user = User.objects.create_user(username="serembamoses", password="moses123")
        self.product = Product.objects.create(
            title="Test Product",
            price=167000,
            category=self.category,
        )

    def test_home_page_view(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)  # Assuming successful response
        self.assertTemplateUsed(response, 'core/index.html')  # Check for used template
       
    def test_category(client):
        response = client.get(reverse("/category/"))
        assert response.status_code == 200
        assertTemplateUsed(response, "core/category.html")
                
        
    # @login_required
    # def mystore(request):
    #     # ...

    def test_mystore_view_login(self):
        response = self.client.get('/mystore/')
        self.assertEqual(response.status_code, 302)  # Redirect for login required
    
    def test_search_view(self):
        query = "test"
        response = self.client.get(f'/search/?query={query}')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'core/search.html')  # Check for used template
        self.assertContains(response, query)  # Check for search query in context
    
    def test_register_user(self):       
        data = {
            'username': 'serembamoses',
            'password1': 'moses1234',
            'password2': 'moses1234',            
        }

        response = self.client.post('/register_user/', data=data)
        self.assertEqual(response.status_code, 302) 
        self.assertRedirected(response, '/')  # Redirect to homepage
        
        user = User.objects.filter(username=data['username']).exists()
        self.assertTrue(user)
   
    def test_login_user(self):        
        data = {
            'username': 'serembamoses',
            'password': 'moses1234',
        }

        response = self.client.post('/login_user/', data=data)
        self.assertEqual(response.status_code, 302)  # Redirect after successful login
        self.assertRedirected(response, '/')  # Redirect to homepage

        # Check if user is authenticated (modify based on your logic)
        self.assertTrue(self.client.session.get('_auth_user_id'))
        
    def test_product_creation_view(self):
        self.client.login(username="serembamoses", password="moses1234") 
    
        data = {
            'title': 'New Product',
            'price': 167000,
            #'category': self.category.id,
            # Add other product form fields
        }

        response = self.client.post('/addproduct/', data=data)
        self.assertEqual(response.status_code, 302)  # Redirect after successful creation
        self.assertRedirected(response, '/core:myproduct/')  # Redirect to user products

        # Check if product is created
        new_product = Product.objects.filter(title=data['title']).exists()
        self.assertTrue(new_product)              
         
      
    def test_user_profile_update(self):
        self.client.login(username="test_user", password="test123") 
        
        data = {
            'first_name': 'Seremba',
            'last_name': 'Moses',            
        }

        response = self.client.post('/update_user/', data=data)
        self.assertEqual(response.status_code, 302)  
        self.assertRedirected(response, '/core:userprofile')  
        
        updated_user = User.objects.get(username="serembamoses")
        self.assertEqual(updated_user.first_name, data['Seremba'])
        self.assertEqual(updated_user.last_name, data['Moses'])
    
            
    def test_cart(client):
        response = client.post(reverse('cart_summary')) 
        assert response.status_code == 200
        assertTemplateUsed(response, "core/cart_summary.html")
                
    def test_checkout(client):
        response = client.get(reverse('checkout'))
        assert response.status_code == 302
        if response.status_code == 200:
            assertTemplateUsed(response, "core/checkout.html")

            

    # def test_payment_successful_view(self):   
    #     response = self.client.get('/payment-success/')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'core/payment-success.html') 
        

    # def test_payment_failed_view(self):  
    #     response = self.client.get('/payment-failed/')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'core/payment-failed.html')  
    
class BusinessRegisterTest(TestCase):

    def setUp(self):
        # Create any necessary data for tests (e.g., dummy user)
        pass
 
        
    def test_invalid_registration_empty(self):
        # Send a POST request with empty data
        response = self.client.post(reverse('core:business_register'), {})

        # Check if form is rendered with errors
        self.assertEqual(response.status_code, 200)  # Renders form template
        self.assertContains(response, "This field is required.")  # Check for error message

        # Check if user is not created
        self.assertEqual(User.objects.count(), 0)  # No user created
