from django.urls import resolve
from core.views import *
from PaymentApp.views import *
from cart.views import *
from django.conf import settings


class TEST_ALL_URLS:
    def test_index_url(self):
        url = resolve('/index/')
        assert url.func == index
        assert url.view_name == 'index'
        
    def test_category_summary_url(self):
        url = resolve('/category_summary/')
        assert url.func == category_summary
        assert url.view_name == 'category_summary'
        
    def test_category_url(self):
        url = resolve('/category/')
        assert url.func == category
        assert url.view_name == 'category'
    
    def test_search_url(self):
        url = resolve('/search/')
        assert url.func == search
        assert url.view_name == 'search'
        
    def test_allproduct_url(self):
        url = resolve('/allproduct/')
        assert url.func == allproduct
        assert url.view_name == 'allproduct'
        
    def test_update_user_url(self):
        url = resolve('/update_user/')
        assert url.func == update_user
        assert url.view_name == 'update_user'
    
    def test_myproduct_url(self):
        url = resolve('/myproduct/')
        assert url.func == myproduct
        assert url.view_name == 'myproduct'
    
    def test_vendor_detail_url(self):
        url = resolve('/vendor_detail/')
        assert url.func == vendor_detail
        assert url.view_name == 'vendor_detail'
    
    def test_business_register_url(self):
        url = resolve('/business_register/')
        assert url.func == business_register
        assert url.view_name == 'business_register'
    
    def test_editproduct_url(self):
        url = resolve('/editproduct/')
        assert url.func == editproduct
        assert url.view_name == 'editproduct'
        
    def test_logout_url(self):
        url = resolve('/logout/')
        assert url.func == logout_user
        assert url.view_name == 'logout_user'
        
    def test_update_password_url(self):
        url = resolve('/update_paasword/')
        assert url.func == update_password
        assert url.view_name == 'update_password'

    def test_login_user_url(self):
        url = resolve('/login/')
        assert url.func == login_user
        assert url.view_name == 'login_user'
        
    def test_deleteProduct_user_url(self):
        url = resolve('/deleteProduct/')
        assert url.func == deleteProduct
        assert url.view_name == 'deleteProduct'
        
    def test_base_url(self):
        url = resolve('/base/')
        assert url.func == base
                
    def test_register_url(self):
        url = resolve('/register/')
        assert url.func == register_user
        assert url.view_name == 'register_user'
        
    def test_add_product_to_store_url(self):
        url = resolve('/mystore/addproduct/')
        assert url.func == addproduct
        assert url.view_name == 'addproduct'
        
    def test_mystore_url(self):
        url = resolve('/mystore/')
        assert url.view_name == 'mystore'
        
    def test_paymnet_success_url(self):
        url = resolve('/PaymmentApp/payment-success/')
        assert url.func == PaymentSuccessful
        assert url.view_name == 'payment-success'
        
    def test_payment_failed_url(self):
        url = resolve('/PaymentApp/payment-failed/')
        assert url.func == paymentFailed
        assert url.view_name == 'payment-failed'
        
    def test_delete_from_cart_url(self):
        url = resolve('/cart/delete/')
        assert url.func == cart_delete
        assert url.view_name == 'cart_delete'
        
    def test_checkout_url(self):
        url = resolve('/PaymentApp/checkout/')
        assert url.func == CheckOut
        assert url.view_name == 'checkout'
        
    def test_cart_add_items_url(self):
        url = resolve('/cart/add/')
        assert url.func == cart_add
        assert url.view_name == 'cart_add'
    
    def test_payment_failed_url(self):
        url = resolve('/payment-failed/')
        assert url.func == CheckOut
        assert url.view_name == 'checkout'
            
    def test_cart_summary_url(self):
        url = resolve('/cart_summary/')
        assert url.func == cart_summary
        assert url.view_name == 'cart_summary'
        
        